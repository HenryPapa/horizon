package br.com.papa.horizon.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.papa.horizon.entity.Cliente;
import br.com.papa.horizon.entity.PecaUtilizada;

public class PecaUtilizadaDao extends GenericDao<PecaUtilizada>{

	public PecaUtilizadaDao() {
		super(PecaUtilizada.class);
	}
	
}
